package com.edu.sjsu.cmpe277.finalprojectteam10;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.edu.sjsu.cmpe277.finalprojectteam10.adapter.EventListDashboardAdapter;
import com.edu.sjsu.cmpe277.finalprojectteam10.adapter.UserInviteListAdapter;
import com.edu.sjsu.cmpe277.finalprojectteam10.adapter.UserListDashBoardAdapter;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiEvent;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.User;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by gyoho on 12/2/15.
 */
public class InviteMemberActivity extends AppCompatActivity {

    private static final String TAG = "InviteMemberActivity";

    private ListView userListView;
    private UserInviteListAdapter userInviteListAdapter;
    private List<String> attendeeList;
    String eventTitle;
    String eventDesc;
    String[] attendees;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_member);

        eventTitle = getIntent().getStringExtra("eventTitle");
        eventDesc = getIntent().getStringExtra("eventDesc");
        attendees = getIntent().getStringArrayExtra("memberNameList");
        attendeeList = Arrays.asList(attendees);

        userListView = (ListView) findViewById(R.id.im_userList);
        createUserList();
    }

    private void loadUsersLists(List<User> users) {
        userInviteListAdapter = new UserInviteListAdapter(this, users);
        userListView.setAdapter(userInviteListAdapter);
        userListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    }

    private List<User> createUserList() {
        final List<User> notAttendingMembers = new ArrayList<>();

        ParseQuery<ParseObject> allUserQuery = ParseQuery.getQuery("User");
        allUserQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> allUserList, ParseException e) {
                if (e == null) {
                    for (ParseObject user : allUserList) {
                        if (!attendeeList.contains(user.getString("name"))) {
                            String name = user.getString("name");
                            String photoUrl = user.getString("photoUrl");
                            String tagline = user.getString("tagline");
                            ArrayList<Double> currentLocation = (ArrayList<Double>) user.get("currentLocation");
                            notAttendingMembers.add(new User(name, photoUrl, tagline, currentLocation));
                        }
                    }
                    loadUsersLists(notAttendingMembers);
                } else {
                    Log.d(TAG, "Error: " + e.getMessage());
                }
            }
        });

        return notAttendingMembers;
    }


    private void addMember() {
        Log.i(TAG, "AddMember func: " + UserInviteListAdapter.checkedUserNameList.size());

        ParseQuery<ParseObject> eventQuery = ParseQuery.getQuery("Event");
        eventQuery.whereEqualTo("title", eventTitle);
        eventQuery.whereEqualTo("description", eventDesc);
        eventQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            // get the event parse object
            public void done(final ParseObject thisEvent, ParseException e) {
                if (thisEvent == null) {
                    Log.d(TAG, "Request to Parse failed.");
                } else {
                    for(String joiningMemberName: UserInviteListAdapter.checkedUserNameList) {
                        // get the joining member parse object
                        ParseQuery<ParseObject> joiningMemberQuery = ParseQuery.getQuery("User");
                        Log.d(TAG, "joiningMemberName: " + joiningMemberName);
                        joiningMemberQuery.whereEqualTo("name", joiningMemberName);
                        joiningMemberQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                            public void done(ParseObject joiningMember, ParseException e) {
                                if (joiningMember == null) {
                                    Log.d(TAG, "No matching result.");
                                } else {
                                    // add member to the event relation
                                    ParseRelation<ParseObject> relation = thisEvent.getRelation("members");
                                    relation.add(joiningMember);
                                    thisEvent.saveInBackground();
                                }
                            }
                        });
                    }

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_invite_member, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_add_member) {
            addMember();

            Intent intent = new Intent(this, HomePageActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
