package com.edu.sjsu.cmpe277.finalprojectteam10.adapter;

import android.widget.TextView;

import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiEvent;

/**
 * Created by gunjaagrawal on 11/22/15.
 */
public class EventViewHolder {
    TextView title;
    TextView description;
    TextView startTime;
    TextView endTime;
    public SkiEvent skiEvent;
}
