package com.edu.sjsu.cmpe277.finalprojectteam10.model;

/**
 * Created by gunjaagrawal on 11/24/15.
 */
public class SkiRecord {
    String eventId;
    String distance;

    public SkiRecord(String eventId, String distance){
        this.eventId = eventId;
        this.distance = distance;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
