package com.edu.sjsu.cmpe277.finalprojectteam10.adapter;

import android.widget.TextView;

import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiRecord;

/**
 * Created by gunjaagrawal on 11/24/15.
 */
public class SkiRecordViewHolder {
    TextView distance;
    TextView eventId;
    public SkiRecord skiRecord;
}
