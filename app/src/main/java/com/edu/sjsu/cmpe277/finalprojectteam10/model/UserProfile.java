package com.edu.sjsu.cmpe277.finalprojectteam10.model;

import java.util.List;

/**
 * Created by shivi on 11/23/15.
 */
public class UserProfile {
    public String Id;
    public String name;
    public String photoUrl;
    public String tagline;
    public List<SkiEvent> events;
}

