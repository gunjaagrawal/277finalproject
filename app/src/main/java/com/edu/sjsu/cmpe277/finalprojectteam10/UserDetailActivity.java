package com.edu.sjsu.cmpe277.finalprojectteam10;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.edu.sjsu.cmpe277.finalprojectteam10.adapter.SkiRecordForUserDetailViewAdapter;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.PermissionUtils;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiRecord;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class UserDetailActivity extends AppCompatActivity implements OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback,GoogleMap.OnMyLocationButtonClickListener{

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;

    private GoogleMap mMap;
    private ArrayList<Double> loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        loc = (ArrayList<Double>) getIntent().getSerializableExtra("location");

        TextView userName = (TextView) findViewById(R.id.udv_userName);
        userName.setText(getIntent().getStringExtra("userName") + "  ");
        TextView tagLine = (TextView) findViewById(R.id.udv_userTagline);
        tagLine.setText(getIntent().getStringExtra("tagLine") + "  ");

        ListView skiRecords = (ListView) findViewById(R.id.udv_skiRecordList);

        List<SkiRecord> dummySkiRecords = createDummySkiRecord();
        SkiRecordForUserDetailViewAdapter skiRecordForUserDetailViewAdapter = new SkiRecordForUserDetailViewAdapter(this, dummySkiRecords);
        skiRecords.setAdapter(skiRecordForUserDetailViewAdapter);
        skiRecords.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        mMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();
    }
    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null && loc != null && !loc.isEmpty()) {
            // Access to the location has been granted to the app.
            CameraUpdate center=
                    CameraUpdateFactory.newLatLng(new LatLng(loc.get(0),
                            loc.get(1)));
            CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);

            Marker m  = mMap.addMarker(new MarkerOptions()

                    .position(new LatLng(loc.get(0),
                            loc.get(1)))

                    .title("test"));

            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
            //mMap.setMyLocationEnabled(true);
        } else {
            //
        }
    }

    public boolean verifyPermissions(int[] grantResults) {
        // At least one result must be checked.
        if(grantResults.length < 1){
            return false;
        }

        // Verify that each required permission has been granted, otherwise return false.
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    private List<SkiRecord> createDummySkiRecord() {
        List<SkiRecord> records = new ArrayList<>();
        records.add(new SkiRecord("Event1", "1.0 mile"));
        records.add(new SkiRecord("Event2", "1.5 mile"));
        records.add(new SkiRecord("Event3", "1.3 mile"));
        records.add(new SkiRecord("Event4", "1.1 mile"));
        records.add(new SkiRecord("Event5", "0.8 mile"));
        records.add(new SkiRecord("Event6", "1.0 mile"));
        records.add(new SkiRecord("Event7", "1.5 mile"));
        records.add(new SkiRecord("Event8", "1.3 mile"));

        return records;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_detail, menu);



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }
}
