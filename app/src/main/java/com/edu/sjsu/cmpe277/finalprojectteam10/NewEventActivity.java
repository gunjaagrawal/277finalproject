package com.edu.sjsu.cmpe277.finalprojectteam10;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

import java.util.Calendar;

/**
 * Created by gyoho on 11/30/15.
 */
public class NewEventActivity extends AppCompatActivity {

    private static final String TAG = "NewEventActivity";

    EditText titleValueText, descriptionValueText, startDateValueText, startTimeValueText, endDateValueText, endTimeValueText;
    Button btnCreate, btnReset;
    EditText startDate, endDate, startTime, EndTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }


    //TODO: repeating code fix me!
    public void selectStartDate(View view) {
        DialogFragment newFragment = new SelectDateStartFragment();
        newFragment.show(getSupportFragmentManager(), "DatePicker");
    }
    public void populateSetStartDate(int year, int month, int day) {
        startDate = (EditText)findViewById(R.id.textStartDateValue);
        startDate.setText(month + "/" + day + "/" + year);
    }


    public class SelectDateStartFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetStartDate(yy, mm + 1, dd);
        }
    }

    public void selectEndDate(View view) {
        DialogFragment newFragment = new SelectDateEndFragment();
        newFragment.show(getSupportFragmentManager(), "DatePicker");
    }
    public void populateSetEndDate(int year, int month, int day) {
        endDate = (EditText)findViewById(R.id.textEndDateValue);
        endDate.setText(month + "/" + day + "/" + year);
    }


    public class SelectDateEndFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetEndDate(yy, mm + 1, dd);
        }
    }



    public void selectStartTime(View view) {
        DialogFragment newFragment = new SelectTimeStartFragment();
        newFragment.show(getSupportFragmentManager(), "TimePicker");
    }

    public class SelectTimeStartFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);



            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hour, int minute) {
            // hour/minute less than 10 should prepend 0
            String hourStr = Integer.toString(hour);
            String minStr = Integer.toString(minute);
            if(hour/10 == 0) {
                hourStr = "0" + hourStr;
            }

            if(minute/10 == 0) {
                minStr = "0" + minStr;
            }
            startTime = (EditText) findViewById(R.id.textStartTimeValue);
            startTime.setText("" + hourStr + ":" + minStr);
        }
    }


    public void selectEndTime(View view) {
        DialogFragment newFragment = new SelectTimeEndFragment();
        newFragment.show(getSupportFragmentManager(), "TimePicker");
    }

    public class SelectTimeEndFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hour, int minute) {
            // hour/minute less than 10 should prepend 0
            String hourStr = Integer.toString(hour);
            String minStr = Integer.toString(minute);
            if(hour/10 == 0) {
                hourStr = "0" + hourStr;
            }
            if(minute/10 == 0) {
                minStr = "0" + minStr;
            }
            startTime = (EditText) findViewById(R.id.textEndTimeValue);
            startTime.setText("" + hourStr + ":" + minStr);
        }
    }



    /** Called when the user clicks the Send button */
    public void createEvent(View view) {
        if(validateInput()) {

            // retrieve current user to create relation
            ParseQuery<ParseObject> userQuery = ParseQuery.getQuery("User");
            userQuery.whereEqualTo("id", MainActivity.USERID);
            userQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                public void done(ParseObject currentUser, ParseException e) {
                    if (e == null) {
                        if (currentUser == null) {
                            Log.e(TAG, "Bug in code");
                        } else {
                            titleValueText = (EditText) findViewById(R.id.textTitleValue);
                            descriptionValueText = (EditText) findViewById(R.id.textDescriptionValue);
                            startDateValueText = (EditText) findViewById(R.id.textStartDateValue);
                            startTimeValueText = (EditText) findViewById(R.id.textStartTimeValue);
                            endDateValueText = (EditText) findViewById(R.id.textEndDateValue);
                            endTimeValueText = (EditText) findViewById(R.id.textEndTimeValue);

                            ParseObject event = new ParseObject("Event");
                            event.put("title", titleValueText.getText().toString());
                            event.put("description", descriptionValueText.getText().toString());
                            event.put("startDate", startDateValueText.getText().toString());
                            event.put("startTime", startTimeValueText.getText().toString());
                            event.put("endDate", endDateValueText.getText().toString());
                            event.put("endTime", endTimeValueText.getText().toString());
                            ParseRelation<ParseObject> relation = event.getRelation("members");
                            relation.add(currentUser);
                            event.saveInBackground();
                        }
                    } else {
                        Log.d(TAG, "Error: " + e.getMessage());
                    }
                }
            });

            Intent intent = new Intent(this, HomePageActivity.class);
            startActivity(intent);
        }
    }

    public void resetField(View view) {
        ViewGroup group = (ViewGroup)findViewById(R.id.fieldView);
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            view = group.getChildAt(i);
            if (view instanceof EditText)
                ((EditText)view).setText("");
        }
    }


    public boolean validateInput() {
        ViewGroup group = (ViewGroup)findViewById(R.id.fieldView);
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View view = group.getChildAt(i);
            if (view instanceof EditText) {
                if(view.isClickable()) {
                    if(((EditText) view).getText().toString().length() == 0) {
                        ((EditText) view).setError("This field is required!");
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
