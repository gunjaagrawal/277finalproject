package com.edu.sjsu.cmpe277.finalprojectteam10.adapter;

import android.widget.TextView;

import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiEvent;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiSession;

/**
 * Created by virajkulkarni on 11/29/15.
 */

public class SessionViewHolder {
    TextView title;
    TextView startTime;
    TextView endTime;
    TextView distanceTraveled;
    public SkiSession skiSession;
}
