package com.edu.sjsu.cmpe277.finalprojectteam10.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edu.sjsu.cmpe277.finalprojectteam10.EventDetailActivity;
import com.edu.sjsu.cmpe277.finalprojectteam10.HomePageActivity;
import com.edu.sjsu.cmpe277.finalprojectteam10.R;
import com.edu.sjsu.cmpe277.finalprojectteam10.UserDetailActivity;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiEvent;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiRecord;

import java.util.List;

/**
 * Created by gunjaagrawal on 11/22/15.
 */
public class SkiRecordForUserDetailViewAdapter extends BaseAdapter {
    private UserDetailActivity activity;
    private LayoutInflater inflater;
    private List<SkiRecord> skiRecords;

    public SkiRecordForUserDetailViewAdapter(UserDetailActivity activity, List<SkiRecord> skiRecords) {
        this.activity = activity;
        this.skiRecords = skiRecords;
    }


    @Override
    public int getCount() {
        return skiRecords.size();
    }

    @Override
    public Object getItem(int position) {
        return skiRecords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SkiRecordViewHolder holder;
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.skirecord_list_row, null);
            holder = new SkiRecordViewHolder();
            holder.eventId = (TextView) convertView.findViewById(R.id.eventId);
            holder.distance = (TextView) convertView.findViewById(R.id.distance);
            convertView.setTag(holder);
        } else {
            holder = (SkiRecordViewHolder) convertView.getTag();
        }

        final SkiRecord skiRecord = skiRecords.get(position);
        holder.skiRecord = skiRecord;
        holder.eventId.setText(skiRecord.getEventId());
        holder.distance.setText("Distance tracked: " + skiRecord.getDistance());

        return convertView;
    }
}
