package com.edu.sjsu.cmpe277.finalprojectteam10.model;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by gunjaagrawal on 11/22/15.
 */
public class User {
    String userId;
    String name;
    Bitmap avatar;
    String avatarUrl;
    String tagline;
    ArrayList<Double> currentLocation;

    public User(String name, String avatarUrl, String tagline, ArrayList<Double> loc) {
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.tagline = tagline;
        this.currentLocation = loc;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Bitmap getAvatar() {
        return avatar;
    }

    public void setAvatar(Bitmap avatar) {
        this.avatar = avatar;
    }

    public ArrayList<Double> getcurrentLocation() {
        return currentLocation;
    }

    public void setcurrentLocation(ArrayList<Double> currentLocation) {
        this.currentLocation = currentLocation;
    }
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }
}