package com.edu.sjsu.cmpe277.finalprojectteam10.adapter;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.CheckBox;

import com.edu.sjsu.cmpe277.finalprojectteam10.model.User;

/**
 * Created by gunjaagrawal on 11/22/15.
 */
public class UserViewHolder {
    TextView name;
    ImageView avatar;
    TextView tagline;
    CheckBox checkBox;
    public User user;
}
