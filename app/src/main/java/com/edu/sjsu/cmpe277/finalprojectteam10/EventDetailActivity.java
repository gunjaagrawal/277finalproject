package com.edu.sjsu.cmpe277.finalprojectteam10;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.ListView;

import com.edu.sjsu.cmpe277.finalprojectteam10.adapter.UserListForEventDetailViewAdapter;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.User;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class EventDetailActivity extends AppCompatActivity {

    private static final String TAG = "EventDetailActivity";
    String eventTitleStr, eventDescStr;
    private ListView userListView;
    private List<User> members;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        TextView eventTitle = (TextView) findViewById(R.id.edv_eventTitleTextView);
        eventTitle.setText(getIntent().getStringExtra("eventTitle"));
        TextView eventDesc = (TextView) findViewById(R.id.edv_eventDescTextView);
        eventDesc.setText(getIntent().getStringExtra("eventDesc"));
        TextView eventStart = (TextView) findViewById(R.id.edv_eventStartTextView);
        eventStart.setText("Start time: " + getIntent().getStringExtra("eventStart"));
        TextView eventEnd = (TextView) findViewById(R.id.edv_eventEndTextView);
        eventEnd.setText("End time: " + getIntent().getStringExtra("eventEnd"));

        // truncate extra white space
        eventTitleStr = eventTitle.getText().toString();
        eventDescStr = eventDesc.getText().toString();

        userListView = (ListView) findViewById(R.id.edv_userList);
        members = createMemberUserList();
        initializeTabView();
    }

    private void initializeTabView() {


        userListView = (ListView) findViewById(R.id.edv_userList);
        List<User> members = createMemberUserList();

        TabHost tabHost = (TabHost) findViewById(R.id.eventTabHost);
        tabHost.setup();

        TabHost.TabSpec spec1 = tabHost.newTabSpec("tabSessionTracking");
        spec1.setContent(R.id.tabSessionTracking);
        spec1.setIndicator("Track");
        tabHost.addTab(spec1);

        TabHost.TabSpec spec2 = tabHost.newTabSpec("tabEventMembers");
        spec2.setContent(R.id.tabEventMembers);
        spec2.setIndicator("Members");
        tabHost.addTab(spec2);
    }

    private void loadUsersLists(List<User> members) {
        UserListForEventDetailViewAdapter userListForEventDetailViewAdapter = new UserListForEventDetailViewAdapter(this, members);
        userListView.setAdapter(userListForEventDetailViewAdapter);
        userListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    }

    private List<User> createMemberUserList() {
        final List<User> members = new ArrayList<>();

        ParseQuery<ParseObject> eventQuery = ParseQuery.getQuery("Event");
        eventQuery.whereEqualTo("title", eventTitleStr);
        eventQuery.whereEqualTo("description", eventDescStr);
        eventQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject thisEvent, ParseException e) {
                if (thisEvent == null) {
                    Log.d(TAG, "Request to Parse failed.");
                } else {
                    ParseRelation relation = thisEvent.getRelation("members");
                    ParseQuery memberQuery = relation.getQuery();
                    memberQuery.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> memberList, ParseException e) {
                            if (e == null) {
                                for(ParseObject member: memberList) {
                                    String name = member.getString("name");
                                    String photoUrl = member.getString("photoUrl");
                                    String tagline = member.getString("tagline");
                                    ArrayList<Double> currentLocation = (ArrayList<Double>)member.get("currentLocation");
                                    members.add(new User(name, photoUrl, tagline, currentLocation));
                                }
                                loadUsersLists(members);

                            } else {
                                Log.d(TAG, "Error: " + e.getMessage());
                            }
                        }
                    });
                }
            }
        });

        return members;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            createMemberUserList();
            return true;
        }

        if (id == R.id.action_invite) {
            List<String> memberNameList = new ArrayList<>();
            for(User member: members) {
                memberNameList.add(member.getName());
            }
            String[] memberNameStrList = memberNameList.toArray(new String[memberNameList.size()]);
            Log.d(TAG, "memberNameStrList: " + memberNameStrList.length);

            Intent intent = new Intent(this, InviteMemberActivity.class);
            intent.putExtra("eventTitle", eventTitleStr);
            intent.putExtra("eventDesc", eventDescStr);
            intent.putExtra("memberNameList", memberNameStrList);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
