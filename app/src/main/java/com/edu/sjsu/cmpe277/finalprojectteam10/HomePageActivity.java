package com.edu.sjsu.cmpe277.finalprojectteam10;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TabHost;

import com.edu.sjsu.cmpe277.finalprojectteam10.adapter.EventListDashboardAdapter;
import com.edu.sjsu.cmpe277.finalprojectteam10.adapter.UserListDashBoardAdapter;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiEvent;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.User;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class HomePageActivity extends AppCompatActivity {

    private static final String TAG = "HomePageActivity";

    private ListView eventListView;
    private ListView userListView;
    private EventListDashboardAdapter eventListDashboardAdapter;
    private UserListDashBoardAdapter userListDashBoardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        initializeTabView();
        initializeLists();
    }

    private void initializeLists() {
        createEventList();
        createUserList();
    }

    private void loadEventsLists(List<SkiEvent> events) {
        eventListDashboardAdapter = new EventListDashboardAdapter(this, events);
        eventListView.setAdapter(eventListDashboardAdapter);
        eventListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    }

    private void loadUsersLists(List<User> users) {
        userListDashBoardAdapter = new UserListDashBoardAdapter(this, users);
        userListView.setAdapter(userListDashBoardAdapter);
        userListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    }

    private List<User> createUserList() {
        final List<User> users = new ArrayList<>();

        ParseQuery<ParseObject> userQuery = ParseQuery.getQuery("User");
        userQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> allUserList, ParseException e) {
                if (e == null) {
                    for (ParseObject userObj : allUserList) {
                        User user = new User(userObj.getString("name"), userObj.getString("photoUrl"), userObj.getString("tagline"), (ArrayList<Double>)userObj.get("currentLocation"));
                        users.add(user);
                    }
                    loadUsersLists(users);
                } else {
                    Log.d(TAG, "Error: " + e.getMessage());
                }
            }
        });

        return users;
    }

    private List<SkiEvent> createEventList() {
        final List<SkiEvent> events = new ArrayList<>();
        // retrieve all event this user belongs to
        ParseQuery<ParseObject> userQuery = ParseQuery.getQuery("User");
        userQuery.whereEqualTo("id", MainActivity.USERID);
        userQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject currentUser, ParseException e) {
                if (e == null) {
                    if (currentUser == null) {
                        Log.e(TAG, "Bug in code");
                    } else {
                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                        query.whereEqualTo("members", currentUser);
                        query.findInBackground(new FindCallback<ParseObject>() {
                            public void done(List<ParseObject> eventList, ParseException e) {
                                if (e == null) {
                                    for (ParseObject event: eventList) {
                                        String title = event.getString("title");
                                        String description = event.getString("description");
                                        String startTime = event.getString("startDate") + " " + event.getString("startTime");
                                        String endTime = event.getString("endDate") + " " + event.getString("endTime");
                                        events.add(new SkiEvent(title, description, startTime, endTime));
                                    }
                                    loadEventsLists(events);
                                } else {
                                    Log.d(TAG, "Error: " + e.getMessage());
                                }
                            }
                        });
                    }
                } else {
                    Log.d(TAG, "Error: " + e.getMessage());
                }
            }
        });

        return events;
    }

    private void initializeTabView() {

        eventListView = (ListView) findViewById(R.id.hp_eventList);
        userListView = (ListView) findViewById(R.id.hp_userList);

        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec2 = tabHost.newTabSpec("tabEvents");
        spec2.setContent(R.id.tabEvents);
        spec2.setIndicator("Events");
        tabHost.addTab(spec2);

        TabHost.TabSpec spec3 = tabHost.newTabSpec("tabUsers");
        spec3.setContent(R.id.tabUsers);
        spec3.setIndicator("Members");
        tabHost.addTab(spec3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_newEvent) {
            Intent intent = new Intent(this, NewEventActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
