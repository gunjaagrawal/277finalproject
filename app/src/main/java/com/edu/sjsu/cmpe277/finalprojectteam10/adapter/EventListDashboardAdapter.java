package com.edu.sjsu.cmpe277.finalprojectteam10.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edu.sjsu.cmpe277.finalprojectteam10.EventDetailActivity;
import com.edu.sjsu.cmpe277.finalprojectteam10.R;
import com.edu.sjsu.cmpe277.finalprojectteam10.HomePageActivity;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.SkiEvent;

import java.util.List;

/**
 * Created by gunjaagrawal on 11/22/15.
 */
public class EventListDashboardAdapter extends BaseAdapter {
    private HomePageActivity activity;
    private LayoutInflater inflater;
    private List<SkiEvent> skiEvents;

    public EventListDashboardAdapter(HomePageActivity mainActivity, List<SkiEvent> skiEvents) {
        this.activity = mainActivity;
        this.skiEvents = skiEvents;
    }


    @Override
    public int getCount() {
        return skiEvents.size();
    }

    @Override
    public Object getItem(int position) {
        return skiEvents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        EventViewHolder holder;
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.skievent_list_row, null);
            holder = new EventViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.description = (TextView) convertView.findViewById(R.id.description);
            holder.startTime = (TextView) convertView.findViewById(R.id.startTime);
            holder.endTime = (TextView) convertView.findViewById(R.id.endTime);
            convertView.setTag(holder);
        } else {
            holder = (EventViewHolder) convertView.getTag();
        }

        final SkiEvent skiEvent = skiEvents.get(position);
        holder.skiEvent = skiEvent;
        holder.title.setText(skiEvent.getTitle());
        holder.description.setText(skiEvent.getDescription());
        holder.startTime.setText("Start: " + skiEvent.getStartTime());
        holder.endTime.setText("End: " + skiEvent.getEndTime());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, EventDetailActivity.class);
                intent.putExtra("eventTitle", skiEvent.getTitle());
                intent.putExtra("eventDesc", skiEvent.getDescription());
                intent.putExtra("eventStart", skiEvent.getStartTime());
                intent.putExtra("eventEnd", skiEvent.getEndTime());
                activity.startActivity(intent);
            }
        });

        return convertView;
    }
}
