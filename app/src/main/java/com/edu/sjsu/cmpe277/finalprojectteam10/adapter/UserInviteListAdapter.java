package com.edu.sjsu.cmpe277.finalprojectteam10.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.edu.sjsu.cmpe277.finalprojectteam10.HomePageActivity;
import com.edu.sjsu.cmpe277.finalprojectteam10.InviteMemberActivity;
import com.edu.sjsu.cmpe277.finalprojectteam10.R;
import com.edu.sjsu.cmpe277.finalprojectteam10.UserDetailActivity;
import com.edu.sjsu.cmpe277.finalprojectteam10.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gunjaagrawal on 11/22/15.
 */
public class UserInviteListAdapter extends BaseAdapter {
    private static final String TAG = "UsrListDashBordAdptr";

    private InviteMemberActivity activity;
    private LayoutInflater inflater;
    private List<User> users;
    public static List<String> checkedUserNameList = new ArrayList<String>();

    public UserInviteListAdapter(InviteMemberActivity activity, List<User> users) {
        this.activity = activity;
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final UserViewHolder holder;
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.member_list_row, null);
            holder = new UserViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.userName);
            holder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
            holder.tagline = (TextView) convertView.findViewById(R.id.tagline);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.ml_checkbox);
            convertView.setTag(holder);
        } else {
            holder = (UserViewHolder) convertView.getTag();
        }

        final User user = users.get(position);
        holder.user = user;
        holder.name.setText(user.getName());
        holder.tagline.setText(user.getTagline());

        // Set thumbnail view
        if (holder.avatar != null) {
            Picasso.with(activity)
                    .load(user.getAvatarUrl())
                    .placeholder(R.drawable.default_placeholder)
                    .into(holder.avatar);
        }


        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((CheckBox) v).isChecked();
                if(checked) {
                    checkedUserNameList.add(holder.name.getText().toString());
                    Log.d(TAG, "added member:" + holder.name.getText().toString());

                } else {
                    checkedUserNameList.remove(holder.name.getText().toString());
                    Log.d(TAG, "removed member:" + holder.name.getText().toString());
                }
            }
        });

        return convertView;
    }
}
